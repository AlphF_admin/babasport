<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">


<title>新巴巴运动网-商品详情页</title>
<link rel="stylesheet" href="/babasport/res/css/style.css" />
<script src="/babasport/res/js/jquery.js"></script>
<script type="text/javascript" src="/babasport/res/js/com.js"></script>
<style type="text/css">
.changToRed {
	border: 2px solid #e4393c;
	padding: 2px 4px;
	float: left;
	margin-right: 4px;
	text-decoration: none;
}
.changToWhite {
	border: 1px solid #ccc;
	padding: 2px 4px;
	float: left;
	margin-right: 4px;
	text-decoration: none;
}
.not-allow {
	cursor: not-allowed;
	color: #999;
	border: 1px dashed #ccc;
	padding: 2px 4px;
	float: left;
	margin-right: 4px;
	text-decoration: none;
}
</style>
</head>
<script type="text/javascript">
	// 初始化执行
	$(function () {
		// 这么写不行？？
		//$("#colors a,#sizes a").on('click', changeFee);这是主方法，on是他们的底层。
		//$("#colors a,#sizes a").bind('click', changeFee);
		// $("#colors a,#sizes a").click(changeFee);// 调用on
		// todo 为啥这么写绑定不上去呢？？
		//$("#colors a,#sizes a").on('click',changeFee)
		//	$("#colors a:first").trigger('click');
	})
	// 数据格式 sku对象的数组
	var skus= $.parseJSON('${skus}');
	var allstock=0;
	skus.forEach(function (sku) {
		allstock += sku.stockInventory;
	})
	// done 不给默认选项，选了哪个下面那个都要联动。
	function colorToRed(target,colorId) {
		var $target = $(target);
		if ($target.attr('class') == "not-allow") return;
		// 如果重新点已选，则取消，并让库存也重新可选
		if ($target.attr('class') == "changToRed"){
			$target.attr('class','changToWhite');
			$("#sizes a.not-allow").attr('class', 'changToWhite');
			return;
		}
		$("#colors a.changToRed").attr('class','changToWhite')
		$target.attr('class','changToRed');

		$("#sizes a.changToWhite").attr('class', 'not-allow');
		// done 用这种标签 页面会复制多段这种代码，这个做法欠妥。
		skus.forEach(function (sku) {
			if (sku.colorId == colorId){
				$("#sizes a#"+sku.size+".not-allow").attr('class', 'changToWhite');
			}
		})
		changeFee()
		<%--<c:forEach items="${skus}" var="sku">
		if ('${sku.colorId}' == colorId){
			$("#sizes a#${sku.size}.not-allow").attr('class', 'changToWhite');
		}
		</c:forEach>--%>
	}
	function sizeToRed(target,size) {
		var $target = $(target);
		if ($target.attr('class') == "not-allow") return;
		// 如果重新点已选，则取消，并让颜色也重新可选
		if ($target.attr('class') == "changToRed"){
			$target.attr('class','changToWhite');
			$("#colors a.not-allow").attr('class', 'changToWhite');
			return;
		}

		$("#sizes a.changToRed").attr('class', 'changToWhite');
		$target.attr('class', 'changToRed');

		// 重新计算可选颜色
		$("#colors a.changToWhite").attr('class', 'not-allow');
		skus.forEach(function (sku) {
			if (sku.size == size){
				$("#colors a#"+sku.colorId+".not-allow").attr('class', 'changToWhite');
			}
		})
		changeFee()
		<%--<c:forEach items="${skus}" var="sku">--%>
		<%--if ('${sku.size}' == size){--%>
			<%--$("#colors a#${sku.colorId}.not-allow").attr('class', 'changToWhite');--%>
		<%--}--%>
		<%--</c:forEach>--%>
	}
	// 改变运费和库存 如果没选就默认所有的库存
	function changeFee(){
		var colorId = $("#colors a.changToRed").attr("id")
		var size = $("#sizes a.changToRed").attr("id")
		if (colorId && size) {
			var deliveFee = 0, stockInventor = 0;
			skus.forEach(function (sku) {
				if (sku.size == size, sku.colorId == colorId) {
					stockInventor = sku.stockInventory;
					deliveFee = sku.deliveFee;
				}
			})

			$("span#deliveFee").html(deliveFee + "元")
			$("span#stockInventory").html(stockInventor)
		} else {
			$("span#deliveFee").html('包邮')
			$("span#stockInventory").html(allstock)
		}
	}
	<%--
		// 选择颜色时变红，默认选中第一个可选的尺码，京东做法 ，不好，容易误买，买错东西！废除。京东现在也是没有不可选了，直接刷新页面查询有没有货。

		function colorToRed2(target,colorId) {
			$("#colors a.changToRed").attr('class','changToWhite')
			$(target).attr('class','changToRed');
			// 标识有已被变红的尺码,即默认选中第一个可选的尺码，不好！
			var flag = false;
			$("#sizes a").attr('class', 'not-allow');
			<c:forEach items="${skus}" var="sku">
				if ('${sku.colorId}' == colorId){
					if(!flag){
						$("#sizes a#${sku.size}.not-allow").attr('class', 'changToRed');
						flag=true;
					}else {
						$("#sizes a#${sku.size}.not-allow").attr('class', 'changToWhite');
					}
				}
			</c:forEach>
		}
		// 转变尺码
		function sizeToRed2(target,size) {
			var $target = $(target);
			if ($target.attr('class') == "not-allow") return;
			$("#sizes a.changToRed").attr('class', 'changToWhite');
			$target.attr('class', 'changToRed');;
		}
	--%>

	//加入购物车
	function addCart(){
		alert("添加购物车成功!");
	}
	//立即购买
	function buy(){
		window.location.href='cart.jsp';
	}
</script>
</head>
<body>
<div class="bar"><div class="bar_w">
	<p class="l">
		<span class="l">
			收藏本网站！北京<a href="#" title="更换">[更换]</a>
		</span>
	</p>
	<ul class="r uls">
		<li class="dev">
			您好,欢迎来到新巴巴运动网！
		</li>
	<li class="dev"><a href="javascript:void(0)" onclick="login()"  title="登陆">[登陆]</a></li>
	<li class="dev"><a href="javascript:void(0)" onclick="register()" title="免费注册">[免费注册]</a></li>
	<li class="dev"><a href="javascript:void(0)" onclick="logout()" title="退出">[退出]</a></li>
	<li class="dev"><a href="javascript:void(0)" onclick="myOrder()" title="我的订单">我的订单</a></li>
	<li class="dev"><a href="#" title="在线客服">在线客服</a></li>
	<li class="dev after"><a href="#" title="English">English</a></li>
	</ul>
</div></div>
<div class="w loc">
	<div class="h-title">
		<div class="h-logo"><a href="http://localhost:8080"><img src="/babasport/res/img/pic/logo-1.png" /></a></div>
	    <div class="h-search">
	      	<input type="text" />
	        <div class="h-se-btn"><a href="#">搜索</a></div>
	    </div>
	</div>
	<dl id="cart" class="cart r">
		<dt><a href="#" title="结算">结算</a>购物车:<b id="">123</b>件</dt>
		<dd class="hidden">
			<p class="alg_c hidden">购物车中还没有商品，赶紧选购吧！</p>
			<h3 title="最新加入的商品">最新加入的商品</h3>
			<ul class="uls">
				<li>
					<a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">
					<img src="/babasport/res/img/pic/p50x50.jpg" alt="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元" /></a>
					<p class="dt"><a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元</a></p>
					<p class="dd">
						<b><var>¥128</var><span>x1</span></b>
						<a href="javascript:void(0);" title="删除" class="del">删除</a>
					</p>
				</li>
				<li>
					<a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">
					<img src="/babasport/res/img/pic/p50x50.jpg" alt="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元" /></a>
					<p class="dt"><a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元</a></p>
					<p class="dd">
						<b><var>¥128</var><span>x1</span></b>
						<a href="javascript:void(0);" title="删除" class="del">删除</a>
					</p>
				</li>
				<li>
					<a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">
					<img src="/babasport/res/img/pic/p50x50.jpg" alt="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元" /></a>
					<p class="dt"><a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元</a></p>
					<p class="dd">
						<b><var>¥128</var><span>x1</span></b>
						<a href="javascript:void(0);" title="删除" class="del">删除</a>
					</p>
				</li>
				<li>
					<a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">
					<img src="/babasport/res/img/pic/p50x50.jpg" alt="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元" /></a>
					<p class="dt"><a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元</a></p>
					<p class="dd">
						<b><var>¥128</var><span>x1</span></b>
						<a href="javascript:void(0);" title="删除" class="del">删除</a>
					</p>
				</li>
				<li>
					<a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">
					<img src="/babasport/res/img/pic/p50x50.jpg" alt="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元" /></a>
					<p class="dt"><a href="#" title="依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元">依琦莲2014瑜伽服套装新 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色 M全场支持货到付款 全网最低价 千人超高好评瑜伽服赶紧抢！全五分好评截图联系客服还返现五元</a></p>
					<p class="dd">
						<b><var>¥128</var><span>x1</span></b>
						<a href="javascript:void(0);" title="删除" class="del">删除</a>
					</p>
				</li>
			</ul>
			<div>
				<p>共<b>5</b>件商品&nbsp;&nbsp;&nbsp;&nbsp;共计<b class="f20">¥640.00</b></p>
				<a href="#" title="去购物车结算" class="inb btn120x30c">去购物车结算</a>
			</div>
		</dd>
	</dl>
</div>

<div class="w ofc mt">
	<div class="l">
		<div class="showPro">
			<div class="big"><a id="showImg" class="cloud-zoom" href="${product.img.url}" rel="adjustX:10,adjustY:-1"><img alt="" src="${product.img.url}"></a></div>
		</div>
	</div>
	<div class="r" style="width: 640px">
		<ul class="uls form">
			<li><h2>依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 性价比最高的瑜伽服 三件套 送胸垫 支持货到付款</h2></li>
			<li><label>巴  巴 价：</label><span class="word"><b class="f14 red mr">￥128.00</b>(市场价:<del>￥150.00</del>)</span></li>
			<li><label>商品评价：</label><span class="word"><span class="val_no val3d4" title="4分">4分</span><var class="blue">(已有888人评价)</var></span></li>
			<li><label>运　　费：</label><span class="word" id="deliveFee">10元</span></li>
			<li><label>库　　存：</label><span class="word" id="stockInventory">100</span><span class="word" >件</span></li>
			<li><label>选择颜色：</label>
				<div id="colors" class="pre spec">
					<c:forEach items="${colors}" var="color">
						<a onclick="colorToRed(this,'${color.id}')" id="${color.id}" class="changToWhite" href="javascript:void(0)" title="${color.name}" ><img width="25" height="25" data-img="1" src="/babasport/res/img/pic/ppp00.jpg" alt="${color.name} "><i>${color.name}</i></a>
					</c:forEach>
				</div>
			</li>
			<li id="sizes"><label>尺　　码：</label>
				<a href="javascript:void(0)" class="changToWhite"  id="S" onclick="sizeToRed(this,'S')">S</a>
				<a href="javascript:void(0)" class="changToWhite"  id="M" onclick="sizeToRed(this,'M')">M</a>
				<a href="javascript:void(0)" class="changToWhite"  id="L" onclick="sizeToRed(this,'L')">L</a>
				<a href="javascript:void(0)" class="changToWhite"  id="XL" onclick="sizeToRed(this,'XL')">XL</a>
				<a href="javascript:void(0)" class="changToWhite"  id="XXL" onclick="sizeToRed(this,'XXL')">XXL</a>
			</li>
			<li><label>我 要 买：</label>
				<a id="sub" class="inb arr" style="border: 1px solid #919191;width: 10px;height: 10px;line-height: 10px;text-align: center;" title="减" href="javascript:void(0);" >-</a>
				<input id="num" type="text" value="1" name="" size="1" readonly="readonly">
				<a id="add" class="inb arr" style="border: 1px solid #919191;width: 10px;height: 10px;line-height: 10px;text-align: center;" title="加" href="javascript:void(0);">+</a></li>
			<li class="submit"><input type="button" value="" class="hand btn138x40" onclick="buy();"/><input type="button" value="" class="hand btn138x40b" onclick="addCart()"/></li>
		</ul>
	</div>
</div>
<div class="w ofc mt">
<div class="l wl">
	<h2 class="h2 h2_l mt"><em title="销量排行榜">销量排行榜</em><cite></cite></h2>
	<div class="box bg_white">
		<ul class="uls x_50x50">
			<li>
				<var class="sfont">1</var>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<var class="sfont">2</var>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<var class="sfont">3</var>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
		</ul>
	</div>
	<h2 class="h2 h2_l mt"><em title="我的浏览记录">我的浏览记录</em><cite></cite></h2>
	<div class="box bg_white">
		<ul class="uls x_50x50">
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
			<li>
				<a href="#" title="富一代" target="_blank" class="pic"><img src="/babasport/res/img/pic/ppp.jpg" alt="依琦莲2014瑜伽服套装新" /></a>
				<dl>
					<!-- dt 8个文字+... -->
					<dt><a href="#" title="依琦莲2014瑜伽服套装新" target="_blank">依琦莲2014瑜伽服套装新</a></dt>
					<dd class="orange">￥120 ~ ￥150</dd>
				</dl>
			</li>
		</ul>
	</div>
	
	<h2 class="h2 h2_l mt"><em title="商家精选">商家精选</em><cite></cite></h2>
	<img src="/babasport/res/img/pic/ad200x75.jpg" alt="" />
</div>
<div class="r wr">
		<h2 class="h2 h2_ch mt"><em>
			<a href="javascript:void(0);" title="商品介绍" rel="#detailTab1" class="here">商品介绍</a>
			<a href="javascript:void(0);" title="规格参数" rel="#detailTab2">规格参数</a>
			<a href="javascript:void(0);" title="包装清单" rel="#detailTab3">包装清单</a></em><cite></cite></h2>
		<div class="box bg_white ofc">
			<div id="detailTab1" class="detail">
				<img src="/babasport/res/img/pic/p800b.jpg" /><img src="/babasport/res/img/pic/p800a.jpg" /><img src="/babasport/res/img/pic/p800c.jpg" /><img src="/babasport/res/img/pic/p800d.jpg" />
			</div>
			
			<div id="detailTab2" style="display:none">
				<strong>服务承诺：</strong><br>
	京东向您保证所售商品均为正品行货，京东自营商品开具机打发票或电子发票。凭质保证书及京东发票，可享受全国联保服务（奢侈品、钟表除外；奢侈品、钟表由京东联系保修，享受法定三包售后服务），与您亲临商场选购的商品享受相同的质量保证。京东还为您提供具有竞争力的商品价格和<a target="_blank" href="http://www.jd.com/help/kdexpress.aspx">运费政策</a>，请您放心购买！
	<br><br>
	注：因厂家会在没有任何提前通知的情况下更改产品包装、产地或者一些附件，本司不能确保客户收到的货物与商城图片、产地、附件说明完全一致。只能确保为原厂正货！并且保证与当时市场上同样主流新品一致。若本商城没有及时更新，请大家谅解！ <br/>
		<strong>权利声明：</strong><br>京东上的所有商品信息、客户评价、商品咨询、网友讨论等内容，是京东重要的经营资源，未经许可，禁止非法转载使用。
	<p><b>注：</b>本站商品信息均来自于合作方，其真实性、准确性和合法性由信息拥有者（合作方）负责。本站不提供任何保证，并不承担任何法律责任。</p>	
				
			</div>

			<div id="detailTab3" class="detail" style="display:none">

	<pre class="f14">
		上衣*1 裤子*1 抹胸*1 包装*1 
	</pre>

			</div>
		</div>

	</div>
</div>

<div class="mode">
	<div class="tl"></div><div class="tr"></div>
	<ul class="uls">
		<li class="first">
			<span class="guide"></span>
			<dl>
			<dt title="购物指南">购物指南</dt>
			<dd><a href="#" title="购物流程">购物流程</a></dd>
			<dd><a href="#" title="购物流程">购物流程</a></dd>
			<dd><a href="#" target="_blank" title="联系客服">联系客服</a></dd>
			<dd><a href="#" target="_blank" title="联系客服">联系客服</a></dd>
			</dl>
		</li>
		<li>
			<span class="way"></span>
			<dl>
			<dt title="支付方式">支付方式</dt>
			<dd><a href="#" title="货到付款">货到付款</a></dd>
			<dd><a href="#" title="在线支付">在线支付</a></dd>
			<dd><a href="#" title="分期付款">分期付款</a></dd>
			<dd><a href="#" title="分期付款">分期付款</a></dd>
			</dl>
		</li>
		<li>
			<span class="help"></span>
			<dl>
			<dt title="配送方式">配送方式</dt>
			<dd><a href="#" title="上门自提">上门自提</a></dd>
			<dd><a href="#" title="上门自提">上门自提</a></dd>
			<dd><a href="#" title="上门自提">上门自提</a></dd>
			<dd><a href="#" title="上门自提">上门自提</a></dd>
			</dl>
		</li>
		<li>
			<span class="service"></span>
			<dl>
			<dt title="售后服务">售后服务</dt>
			<dd><a href="#" target="_blank" title="售后策略">售后策略</a></dd>
			<dd><a href="#" target="_blank" title="售后策略">售后策略</a></dd>
			<dd><a href="#" target="_blank" title="售后策略">售后策略</a></dd>
			<dd><a href="#" target="_blank" title="售后策略">售后策略</a></dd>
			</dl>
		</li>
		<li>
			<span class="problem"></span>
			<dl>
			<dt title="特色服务">特色服务</dt>
			<dd><a href="#" target="_blank" title="夺宝岛">夺宝岛</a></dd>
			<dd><a href="#" target="_blank" title="夺宝岛">夺宝岛</a></dd>
			<dd><a href="#" target="_blank" title="夺宝岛">夺宝岛</a></dd>
			<dd><a href="#" target="_blank" title="夺宝岛">夺宝岛</a></dd>
			</dl>
		</li>
	</ul>
</div>
</body>
</html>