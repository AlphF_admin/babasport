package cn.itcast.core.service;

import cn.itcast.core.bean.TestTb;
import cn.itcast.core.dao.TestTbDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class TestTbService {
	// 写这个也行 @Resource
	@Autowired
	private TestTbDao testTbDao;

//	@Override
	public void addTestTb(TestTb testTb){
		testTbDao.addTestTb(testTb);
		//测试事物
		//throw new RuntimeException("11");

	}
}
