package cn.itcast.core.controller.admin;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 后台中心管理
 * 测试SpringMVC
 */
@Controller
@RequestMapping(value = "/control")
public class CenterController {

	@RequestMapping(value = "/{actionName}.do")
	public String index(@PathVariable String actionName){
		return actionName;
	}
	/*@RequestMapping(value = "/index.do")
	public String index(){
		return "index";
	}

	@RequestMapping(value = "/main.do")
	public String main(){
		return "main";
	}
	@RequestMapping(value = "/top.do")
	public String top(){
		return "top";
	}
	@RequestMapping(value = "/left.do")
	public String left(){
		return "left";
	}
	@RequestMapping(value = "/right.do")
	public String right(){
		return "right";
	}*/


//	@InitBinder
//	public void initBinder(WebDataBinder dataBinder, WebRequest request){
//		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		dataBinder.registerCustomEditor(Date.class,new CustomDateEditor(dateformat,true));
//	}


}
