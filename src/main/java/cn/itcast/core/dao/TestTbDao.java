package cn.itcast.core.dao;

import cn.itcast.core.bean.TestTb;
//import org.springframework.stereotype.Component;

/**
 * 测试
 * @author lx
 *
 */
//@Component 由mybatis自己扫描， 不让spring扫到
public interface TestTbDao {

	public void addTestTb(TestTb testTb);
}
