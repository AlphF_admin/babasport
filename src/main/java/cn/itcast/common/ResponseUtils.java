package cn.itcast.common;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseUtils {
	public static void render(HttpServletResponse response,String contentType,String text){
		response.setContentType(contentType);
		try {
			response.getWriter().write(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void renderJson(HttpServletResponse response,String text){
		render(response,"appliction/json;charset=utf-8",text);
	}

}
