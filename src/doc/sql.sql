#create database babasport default character set UTF8;
use babasport;
CREATE TABLE `test_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
select * from test_tb;

#########################品牌##################################################
# 创建品牌表 : bbs_brand
DROP TABLE IF EXISTS `bbs_brand`;
CREATE TABLE `bbs_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) NOT NULL COMMENT '名称',
  `description` varchar(80) DEFAULT NULL COMMENT '描述',
  `img_url` varchar(80) DEFAULT NULL COMMENT '图片Url',
  `web_site` varchar(80) DEFAULT NULL COMMENT '品牌网址',
  `sort` int(11) DEFAULT NULL COMMENT '排序:最大最排前',
  `is_display` tinyint(1) DEFAULT NULL COMMENT '是否可见 1:可见 0:不可见',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='品牌';
# 品牌数据
INSERT INTO bbs_brand VALUES ('1', '依琦莲', null, null, null, '99', '1');
INSERT INTO bbs_brand VALUES ('2', '凯速（KANSOON）', null, null, null, 98, '1');
INSERT INTO bbs_brand VALUES ('3', '梵歌纳（vangona）', null, null, null, 97, '1');
INSERT INTO bbs_brand VALUES ('4', '伊朵莲', null, null, null, 96, '1');
INSERT INTO bbs_brand VALUES ('5', '菩媞', null, null, null, 95, '1');
INSERT INTO bbs_brand VALUES ('6', '丹璐斯', null, null, null, 94, '1');
INSERT INTO bbs_brand VALUES ('7', '喜悦瑜伽', null, null, null, 93, '1');
INSERT INTO bbs_brand VALUES ('8', '皮尔（pieryoga）', null, null, null, 92, '1');
INSERT INTO bbs_brand VALUES ('9', '路伊梵（LEFAN）', null, null, null, 91, '1');
INSERT INTO bbs_brand VALUES ('10', '金啦啦', null, null, null, 90, '0');
INSERT INTO bbs_brand VALUES ('11', '来尔瑜伽（LaiErYoGA）', null, null, null, 89, '1');
INSERT INTO bbs_brand VALUES ('12', '艾米达（aimida）', null, null, null, 88, '1');
INSERT INTO bbs_brand VALUES ('13', '斯泊恩', null, null, null, 87, '1');
INSERT INTO bbs_brand VALUES ('14', '康愫雅KSUA', null, null, null, 86, '1');
INSERT INTO bbs_brand VALUES ('15', '格宁', null, null, null, 85, '1');
INSERT INTO bbs_brand VALUES ('16', 'E奈尔（Enaier）', null, null, null, 84, '1');
INSERT INTO bbs_brand VALUES ('17', '哈他', null, null, null, 83, '1');
INSERT INTO bbs_brand VALUES ('18', '伽美斯（Jamars）', null, null, null, 82, '1');
INSERT INTO bbs_brand VALUES ('19', '瑜伽树（yogatree）', null, null, null, 81, '1');
INSERT INTO bbs_brand VALUES ('20', '兰博伊人（lanboyiren）', null, null, null, 80, '1');

-- 查询
select id,name,description,img_url,web_site,sort,is_display
FROM bbs_brand WHERE is_display = 1;
-- 插入
insert into bbs_brand
(name,description,img_url,web_site,sort,is_display)
values('随便','gegge','','',79,'1');

-- 删除
DELETE FROM  bbs_brand
WHERE id = 24;

-- 查询
select id,name,description,img_url,web_site,sort,is_display
FROM bbs_brand WHERE
  id = 24;

-- 编辑(改)
UPDATE  bbs_brand set description="方法" WHERE id = 24


###############################商品类型##############################################
-- bbs_type
DROP TABLE IF EXISTS `bbs_type`;
CREATE TABLE `bbs_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(36) NOT NULL COMMENT '名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父ID',
  `note` varchar(200) DEFAULT NULL COMMENT '备注,用于google搜索页面描述',
  `is_display` tinyint(1) NOT NULL COMMENT '是否可见 1:可见 0:不可见',
  PRIMARY KEY (`id`),
  KEY `FKA8168A929B5A332` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='商品类型';

INSERT INTO bbs_type VALUES ('1', '瑜珈', '0', null, '0');
INSERT INTO bbs_type VALUES ('2', '瑜珈服', '1', null, '1');
INSERT INTO bbs_type VALUES ('3', '瑜伽辅助', '1', null, '1');
INSERT INTO bbs_type VALUES ('4', '瑜伽铺巾', '1', null, '1');
INSERT INTO bbs_type VALUES ('5', '瑜伽垫', '1', null, '1');
INSERT INTO bbs_type VALUES ('6', '舞蹈鞋服', '1', null, '1');
INSERT INTO bbs_type VALUES ('7', '其它', '1', null, '1');

##########################商品########################################################
-- bbs_product
DROP TABLE IF EXISTS `bbs_product`;
CREATE TABLE `bbs_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `no` varchar(30) DEFAULT NULL COMMENT '商品编号',
  `name` varchar(255) NOT NULL COMMENT '商品名称',
  `weight` double(11,0) DEFAULT NULL COMMENT '重量 单位:克',
  `is_new` tinyint(1) DEFAULT NULL COMMENT '是否新品:0:旧品,1:新品',
  `is_hot` tinyint(1) DEFAULT NULL COMMENT '是否热销:0,否 1:是',
  `is_commend` tinyint(1) DEFAULT NULL COMMENT '推荐 1推荐 0 不推荐',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `create_user_id` varchar(255) DEFAULT NULL COMMENT '添加人ID',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `check_user_id` varchar(255) DEFAULT NULL COMMENT '审核人ID',
  `is_show` tinyint(1) DEFAULT NULL COMMENT '上下架:0否 1是',
  `is_del` tinyint(1) DEFAULT NULL COMMENT '是否删除:0删除,1,没删除',
  `type_id` int(11) DEFAULT NULL COMMENT '类型ID',
  `brand_id` int(11) DEFAULT NULL COMMENT '品牌ID',
  `keywords` varchar(255) DEFAULT NULL COMMENT '检索关键词',
  `sales` int(11) DEFAULT NULL COMMENT '销量',
  `description` longtext COMMENT '商品描述',
  `package_list` longtext COMMENT '包装清单',
  `feature` varchar(255) DEFAULT NULL COMMENT '商品属性集',
  `color` varchar(255) DEFAULT NULL COMMENT '颜色集',
  `size` varchar(255) DEFAULT NULL COMMENT '尺寸集',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `bbs_product_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `bbs_type` (`id`),
  CONSTRAINT `bbs_product_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `bbs_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=279 DEFAULT CHARSET=utf8 COMMENT='商品';

INSERT INTO bbs_product VALUES ('252', '20141028114308048', '252--依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 ', '1', '0', '1', '0', '2014-10-28 11:43:08', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" alt=\"\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('253', '20141028114322284', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:22', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('254', '20141028114328904', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:28', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('255', '20141028114331167', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:31', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('256', '20141028114333647', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:33', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('257', '20141028114336280', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:36', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('258', '20141028114338422', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:38', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('259', '20141028114340470', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:40', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('260', '20141028114342622', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:42', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('261', '20141028114347654', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:47', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('262', '20141028114350662', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:50', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('263', '20141028114352709', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:52', null, null, null, '0', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('264', '20141028114354902', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:54', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('265', '20141028114356998', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:56', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('266', '20141028114358873', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:43:58', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('267', '20141028114401153', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:01', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('268', '20141028114403414', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:03', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('269', '20141028114405217', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:05', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('270', '20141028114407438', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:07', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('271', '20141028114409502', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:09', null, null, null, '1', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');
INSERT INTO bbs_product VALUES ('272', '20141028114411609', '依琦莲2014瑜伽服套装新款 瑜珈健身服三件套 广场舞蹈服装 女瑜伽服送胸垫 长袖紫色', '1', '0', '1', '0', '2014-10-28 11:44:11', null, null, null, '0', '1', '2', '1', null, null, '<p><img width=\"750\" height=\"674\" alt=\"\" src=\"http://localhost:8088/image-web/upload/20141028114251971945586.jpg\" /></p>', '衣服 裤子', '1,2,9,11', '9,11,18,19,29', 'S,M,L,XL');

##################商品图片############################################################
-- bbs_img
DROP TABLE IF EXISTS `bbs_img`;
CREATE TABLE `bbs_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `url` varchar(80) DEFAULT NULL COMMENT '图片URL',
  `is_def` tinyint(1) DEFAULT NULL COMMENT '是否默认:0否 1:是',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `bbs_img_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `bbs_product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COMMENT='图片';


INSERT INTO bbs_img VALUES ('216', '252', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('217', '253', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('218', '254', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('219', '255', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('220', '256', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('221', '257', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('222', '258', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('223', '259', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('224', '260', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('225', '261', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('226', '262', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('227', '263', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('228', '264', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('229', '265', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('230', '266', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('231', '267', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('232', '268', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('233', '269', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('234', '270', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('235', '271', 'res/img/pic/ppp.jpg', '1');
INSERT INTO bbs_img VALUES ('236', '272', 'res/img/pic/ppp.jpg', '1');

-- ----------------------------
-- Table structure for bbs_color
-- ----------------------------
DROP TABLE IF EXISTS `bbs_color`;
CREATE TABLE `bbs_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) DEFAULT NULL COMMENT '名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父ID为色系',
  `img_url` varchar(50) DEFAULT NULL COMMENT '颜色对应的衣服小图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='颜色大全';

-- ----------------------------
-- Records of bbs_color
-- ----------------------------
INSERT INTO `bbs_color` VALUES ('1', '红色系', '0', '365756');
INSERT INTO `bbs_color` VALUES ('2', '绿色系', '0', '831162');
INSERT INTO `bbs_color` VALUES ('3', '蓝色系', '0', '397817');
INSERT INTO `bbs_color` VALUES ('4', '黑色系', '0', '798685');
INSERT INTO `bbs_color` VALUES ('5', '粉色系', '0', '714774');
INSERT INTO `bbs_color` VALUES ('6', '紫色系', '0', '271247');
INSERT INTO `bbs_color` VALUES ('7', '灰色系', '0', '215711');
INSERT INTO `bbs_color` VALUES ('8', '其他', '0', '421753');
INSERT INTO `bbs_color` VALUES ('9', '西瓜红', '1', '361823');
INSERT INTO `bbs_color` VALUES ('10', '大红', '1', '761156');
INSERT INTO `bbs_color` VALUES ('11', '墨绿', '2', '829598');
INSERT INTO `bbs_color` VALUES ('12', '草绿', '2', '666929');
INSERT INTO `bbs_color` VALUES ('13', '青绿', '2', '835412');
INSERT INTO `bbs_color` VALUES ('14', '海军蓝', '3', '463347');
INSERT INTO `bbs_color` VALUES ('15', '海军白蓝条', '14', '134458');
INSERT INTO `bbs_color` VALUES ('16', '紫黑', '4', '839387');
INSERT INTO `bbs_color` VALUES ('17', '纯黑', '4', '926366');
INSERT INTO `bbs_color` VALUES ('18', '浅粉', '5', '385183');
INSERT INTO `bbs_color` VALUES ('19', '浅紫', '6', '235837');
INSERT INTO `bbs_color` VALUES ('20', '浅灰', '7', '379694');
INSERT INTO `bbs_color` VALUES ('21', '均色', '8', '694256');
INSERT INTO `bbs_color` VALUES ('22', '桃粉', '5', '288715');
INSERT INTO `bbs_color` VALUES ('23', '深紫', '6', '448623');
INSERT INTO `bbs_color` VALUES ('24', '浅蓝', '3', '773343');
INSERT INTO `bbs_color` VALUES ('25', '深蓝', '3', '451347');
INSERT INTO `bbs_color` VALUES ('26', '夕阳红', '1', '971962');
INSERT INTO `bbs_color` VALUES ('27', '深灰', '7', '562622');
INSERT INTO `bbs_color` VALUES ('28', '卡其灰', '7', '397751');
INSERT INTO `bbs_color` VALUES ('29', '典雅灰', '7', '274621');


-- ----------------------------
-- Table structure for bbs_feature
-- ----------------------------
DROP TABLE IF EXISTS `bbs_feature`;
CREATE TABLE `bbs_feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '前台排序',
  `is_del` tinyint(1) DEFAULT NULL COMMENT '是否废弃:1:未废弃,0:废弃了',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='属性表';

-- ----------------------------
-- Records of bbs_feature
-- ----------------------------
INSERT INTO `bbs_feature` VALUES ('1', '环保人棉', '环保人棉', '1', '1');
INSERT INTO `bbs_feature` VALUES ('2', '莫代尔', '莫代尔', '2', '1');
INSERT INTO `bbs_feature` VALUES ('3', 'TPE', 'TPE', '3', '1');
INSERT INTO `bbs_feature` VALUES ('4', '棉麻', '棉麻', '4', '1');
INSERT INTO `bbs_feature` VALUES ('5', '锦纶', '锦纶', '5', '1');
INSERT INTO `bbs_feature` VALUES ('6', '竹纤维', '竹纤维', '6', '1');
INSERT INTO `bbs_feature` VALUES ('7', '莱卡棉', '莱卡棉', '7', '1');
INSERT INTO `bbs_feature` VALUES ('8', '牛奶丝', '牛奶丝', '8', '1');
INSERT INTO `bbs_feature` VALUES ('9', '涤纶', '涤纶', '9', '1');
INSERT INTO `bbs_feature` VALUES ('10', '橡胶', '橡胶', '10', '1');
INSERT INTO `bbs_feature` VALUES ('11', 'PVC', 'PVC', '11', '1');
INSERT INTO `bbs_feature` VALUES ('12', '其它', '其它', '12', '1');

-- ----------------------------
-- Table structure for bbs_img
-- ----------------------------
DROP TABLE IF EXISTS `bbs_img`;
CREATE TABLE `bbs_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `url` varchar(80) DEFAULT NULL COMMENT '图片URL',
  `is_def` tinyint(1) DEFAULT NULL COMMENT '是否默认:0否 1:是',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `bbs_img_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `bbs_product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COMMENT='图片';

-- ----------------------------
-- Records of bbs_img
-- ----------------------------
INSERT INTO `bbs_img` VALUES ('216', '252', 'res/img/pic/ppp.jpg', '1');

-- ----------------------------
-- Table structure for bbs_sku
-- ----------------------------
DROP TABLE IF EXISTS `bbs_sku`;
CREATE TABLE `bbs_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_id` int(11) NOT NULL COMMENT '商品ID',
  `color_id` int(11) DEFAULT NULL COMMENT '颜色ID',
  `size` varchar(5) DEFAULT NULL COMMENT '尺码',
  `delive_fee` double DEFAULT NULL COMMENT '运费 默认10元',
  `sku_price` double(20,2) NOT NULL COMMENT '售价',
  `stock_inventory` int(5) NOT NULL COMMENT '库存',
  `sku_upper_limit` int(5) DEFAULT NULL COMMENT '购买限制',
  `location` varchar(80) DEFAULT NULL COMMENT '仓库位置:货架号',
  `sku_img` varchar(80) DEFAULT NULL COMMENT 'SKU图片  精确到颜色及尺码对应的图片',
  `sku_sort` int(5) DEFAULT NULL COMMENT '前台显示排序',
  `sku_name` varchar(500) DEFAULT NULL COMMENT 'SKU名称',
  `market_price` double(20,2) DEFAULT NULL COMMENT '市场价',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` varchar(255) DEFAULT NULL,
  `update_user_id` varchar(255) DEFAULT NULL,
  `last_status` int(1) DEFAULT NULL COMMENT '0,历史 1最新',
  `sku_type` int(1) DEFAULT NULL COMMENT '0:赠品,1普通',
  `sales` int(10) DEFAULT NULL COMMENT '销量',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `color_id` (`color_id`),
  CONSTRAINT `bbs_sku_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `bbs_product` (`id`),
  CONSTRAINT `bbs_sku_ibfk_2` FOREIGN KEY (`color_id`) REFERENCES `bbs_color` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=516 DEFAULT CHARSET=utf8 COMMENT='最小销售单元';

-- ----------------------------
-- Records of bbs_sku
-- ----------------------------
INSERT INTO `bbs_sku` VALUES ('52', '252', '9', 'S', '10', '136.00', '55', '6', null, null, null, null, '150.00', '2014-10-28 11:43:08', null, null, null, null, '1', '0');