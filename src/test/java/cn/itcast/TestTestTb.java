package cn.itcast;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.itcast.common.junit.SpringJunitTest;
import cn.itcast.core.bean.TestTb;
import cn.itcast.core.service.TestTbService;

import java.io.File;
import java.io.IOException;

/**
 * 测试
 */

public class TestTestTb extends SpringJunitTest{

	@Autowired
	private TestTbService testTbService;
	@Test
	public void testAdd() throws Exception {
		TestTb testTb = new TestTb();
		testTb.setName("金乐乐");

		testTbService.addTestTb(testTb);
	}

//	@Test
//	public void testJava(){
//		int[] arr = new int[10];
//		System.out.println(arr);
//	}

	/**
	 * 测试jersey 用于上传图片到服务器
	 */
//	@Test
//	public void testJersey(){
//		// 实例化一个 Jersey
//		Client client = new Client();
//		// 另一台服务器的请求路径, 发送的目录
//		String url = "http://localhost:8088/image-web/upload/ludashi000.jpg";
//		// 设置请求路径
//		WebResource resource = client.resource(url);
//		// 本地路径
//		String path = "C:\\Users\\jiangfeng\\Pictures\\Saved Pictures\\ludashi.jpg";
//		// 	读图片到内存中
//
//		byte[] readFileToByteArray=null;
//		try {
//			 readFileToByteArray = FileUtils.readFileToByteArray(new File(path));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		// 发送开始， 通过put方式发送
//		resource.put(String.class,readFileToByteArray);
//		System.out.printf("发送完毕！");
//	}
}
